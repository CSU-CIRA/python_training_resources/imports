1.  Take a look at Python’s [built-in packages](https://docs.python.org/3/library/index.html) and skim the documentation for anything that looks interesting

2.  Open an interactive Python session and use the help() and dir() functions on anything you found interesting in 1.
    
3.  Create a module named hw_functions
	- Make a function in hw_functions named hello_modules that prints “Hello modules!”
	- In the same directory as hw_functions, create a module named app.
	- Inside app, import hw_functions and call the hello_modules function.
	- Execute app from the command-line.

4.  Put app and hw_functions in a package named hw_package.
	- Place the app and hw_functions modules in hw_package 
	- Correct the imports
	- Make hw_package executable and call app’s main function
	- Execute hw_package from the command-line
