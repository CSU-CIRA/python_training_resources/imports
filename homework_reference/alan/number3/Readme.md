### Create a module named hw_functions


 - [x] Make a function in hw_functions named hello_modules that prints “Hello modules!”
 - [x] In the same directory as hw_functions, create a module named app.
 - [x] Inside app, import hw_functions and call the hello_modules function.
 - [x] Execute app from the command-line.


```
$ python app.py 
Hello modules!
```