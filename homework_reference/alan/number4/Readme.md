### Put app and hw_functions in a package named hw_package.

 - [x] Place the app and hw_functions modules in hw_package
 - [x] Correct the imports
 - [x] Make hw_package executable and call app’s main function
 - [x] Execute hw_package from the command-line

### Install
`pip install -e .`  
Installs "executable" command `run_homework` for quick entry. 
```
$ run_homework 
Hello modules!
```