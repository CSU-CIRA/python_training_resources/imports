""" Incredible 'app' that prints a string to the console """
from hw_package import hw_functions


def run_app():
    """ Driver function triggered from command line execution
    """
    hw_functions.hello_modules()


if __name__ == "__main__":
    run_app()
