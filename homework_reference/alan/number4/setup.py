#!/usr/bin/env python

from distutils.core import setup

setup(name='hwpackage',
      version='1.0',
      description='python tutorial 2021-02-19',
      author='Alan Brammer',
      author_email='dontemailme',
      packages=['hw_package', ],
      entry_points = {
        'console_scripts': ['run_homework=hw_package.app:run_app'],
    }
     )